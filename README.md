# JSTools

This is a short module containing JS procedures that I find myself using on almost all my projects. Some I found on the SB forum, some on stack overflow and some I wrote myself.

### Compatibility
Currently compatible with SB 2.20

### Procedure list

|       Procedure       |                                               Functionnality                                          | Cordova | Web |
|-----------------------|:-----------------------------------------------------------------------------------------------------:|:-------:|:---:|
| b64EncodeUnicode      |Encode an unicode string in base 64.                                                                   |    ✔    |  ✔ | 
| b64DecodeUnicode      |Decode an unicode string in base 64.                                                                   |    ✔    |  ✔ | 
| BindCordovaEvent      |Bind a Cordova event from a callback.                                                                  |    ✔    |  ✖ | 
| UnBindCordovaEvent    |Unbind a Cordova event from a callback.                                                                |    ✔    |  ✖ | 
| FreeDebugOutput       |Actually close the debug window to avoid background window misalignment.                               |    ✔    |  ✔ | 
| GadgetElement         |Get the DOM element for a gadget.                                                                      |    ✔    |  ✔ | 
| GetLocalLanguage      |Will return the user language as a string (EN, FR, etc...).                                            |    ✔    |  ✔ | 
| IsThisCordova         |Will return true if executed in Cordova and false otherwise.                                           |    ✔    |  ✔ | 
| LoadCSS               |Load a CSS. Callback should be : callback(Tag,Result,URL,Error).                                       |    ✔    |  ✔ | 
| LoadScript            |Load a js file. Callback should be : callback(Tag,Result,URL,Error).                                   |    ✔    |  ✔ | 
| SetClipboardText      |Stores a string into the clipboard. If the clipboard already contains text, it will be overwritten.    |    ✔    |  ✔ | 
| uuidv4                |Generate an UUID compliant with RFC 4122.                                                              |    ✔    |  ✔ | 
| WindowElement         |Get the DOM element for a window.                                                                      |    ✔    |  ✔ | 

### Licence
JSTools is released under the [wtfpl licence](http://www.wtfpl.net/).