# Change log

## [v1.0.4]
- :heavy_plus_sign: Added BindCordovaEvent() and UnBindCordovaEvent() to manage Cordova Events in SB. See https://cordova.apache.org/docs/en/latest/cordova/events/events.html for more informations.

## [v1.0.3]
- :page_with_curl: Small clean up, changelog, readme and release on GitLab.
- :wavy_dash: Changed DestroyDebugOutput() to FreeDebugOutput() for consistance.

## [v1.0.2]
- :heavy_plus_sign: Added SetClipboardText(text.s) : Stores a string into the clipboard. If the clipboard already contains text, it will be overwritten.

## [v1.0.1]
- :heavy_plus_sign: Added DestroyDebugOutput() : CloseDebugOutput() only hide the debug window. when associated with PB_Window_Background, it can result in some misalignment.

## [1.0.0]
- :page_with_curl: Original release here : https://forums.spiderbasic.com/viewtopic.php?f=9&p=5415#p5398